<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//to access with the authenticated user
use Illuminate\Support\Facades\Auth;
use App\Models\Post;
use App\Models\PostLike;
use App\Models\PostComment;

class PostController extends Controller
{
    //action to return a view containing a form for post creation
    public function create(){
        return view('posts.create');    
    }
    
    //action to receive the form data and subsequently store said data in the post table
    public function store(Request $request){
        //check if there is an authenticated user
        if(Auth::user()){
            //instantiate a new post object from the Post model
            $post = new Post;
            //define the properties of the $post object using the received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->user_id = (Auth::user()->id);
            //save this post object in the database
            $post->save();

            return redirect('/posts');
        }
        else{
            return redirect('/login');
        }
    }

    public function index(){
        $posts = Post::where('isActive', true)->get();
        //The 'with()' method allows us to pass information from the controller to view page
        return view('posts.index')->with('posts', $posts);
    }

    //Activity for s02
    public function welcome(){
        $posts = Post::where('isActive', true)->inRandomOrder()->limit(3)->get();
        return view('welcome')->with('posts', $posts);
    }

    public function myPost(){
        if(Auth::user()){
            //We are able to fetch the posts related to a specific user because of the established relationship between the models
            $posts = Auth::user()->posts;
            return view('posts.index')->with('posts', $posts);
        }else{
            return redirect('/login');
        }
    }

    //action that will return a view showing the specific post using the URL parameter $id to query for the database entry
    public function show($id){
        $post = Post::find($id);
        return view('posts.show')->with('post', $post);
    }


    //Activity s03
    public function edit($id)
    {
        if(Auth::user()){
            $post = Auth::user()->posts()->find($id);
            if($post){
                return view('posts.edit')->with('post', $post);
            }
            else{
                return redirect('/posts');
            }
        }
        else{
            return redirect('/login');
        }        
    }

    public function update(Request $request, $id){
        $post = Post::find($id);
        //if authenticated user's ID is the same with the post's user_id
        if(Auth::user()->id == $post->user_id){
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->save();
        }
        return redirect('/posts');
    }

    // public function destroy($id){
    //     $post = Post::find($id);
    //     if(Auth::user()->id == $post->user_id){
    //         $post->delete();
    //     }
    //     return redirect('/posts');
    // }
    public function archive($id){
        $post = Post::find($id);
        if(Auth::user()->id == $post->user_id){
            $post->isActive = false;
            $post->save();
        }
        return redirect('/posts');
    }

    public function like($id){
        $post = Post::find($id);

        if(Auth::user()){
            $user_id = Auth::user()->id;
            
            if($user_id != $post->user_id){

                if($post->likes->contains("user_id", $user_id)){
                    PostLike::where('post_id', $post->id)
                    ->where('user_id', $user_id)
                    ->delete();
                }else{
                    $postLike = new PostLike;
                    $postLike->post_id = $post->id;
                    $postLike->user_id = $user_id;
                    $postLike->save();

                }
            }

            return redirect("/posts/$id");
            
        }
        else{
            return redirect('/login');
        }
    }

    public function comment(Request $request, $id){
        $post = Post::find($id);
        if(Auth::user()){
            $user_id = Auth::id();

            $postComment = new PostComment;
            $postComment->content = $request->input('comment');
            $postComment->post_id = $post->id;
            $postComment->user_id = $user_id;
            $postComment->save();
            return redirect("/posts/$id");
        }

        else{
            return redirect('/login');
        }

    }
}
