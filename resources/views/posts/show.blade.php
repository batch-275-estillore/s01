@extends('layouts.app')


@section('content')
<div class="card">
	<div class="card-body">
		<h2 class="card-title">{{$post->title}}</h2>
		<p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
		<p class="card-subtitle text-muted mb-3">Created at: {{$post->created_at}}</p>
		<p class="card-text">{{$post->content}}</p>

		<p class="card-subtitle text-muted mb-3">Likes: {{count($post->likes)}} | Comments: {{count($post->comments)}}</p>

		@if(Auth::user())
		@if(Auth::id() != $post->user_id)
		<form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
			@method('PUT')
			@csrf
			@if($post->likes->contains("user_id", Auth::id()))
			<button type="submit" class="btn btn-danger">Unlike</button>
			@else
			<button type="submit" class="btn btn-success">Like</button>
			@endif
		</form>
		@endif
		<!-- Button trigger modal -->
		<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
			Comment
		</button>
		@endif
		<div class="mt-3">
			<a href="/posts" class="card-link">View All Posts</a>
		</div>
	</div>
</div>

{{-- Comment Section for the s05 Activity --}}
@if(count($post->comments)>0)
<h3 class="my-3">Comments:</h3>
@foreach($post->comments as $comment)

<div class="card my-2">
	<div class="card-body">
		<h3 class="card-title text-center">{{$comment->content}}</h3>
	</div>
	<div class="card-body text-end">
		<h5>Posted by: {{$comment->user->name}}</h5>
		<h6 class="text-muted">Posted on: {{$comment->created_at}}</h6>	
	</div>
		
	
</div>
@endforeach
@endif


<!-- Modal -->
	<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="staticBackdropLabel">Leave a Comment</h5>
					<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
				</div>
				<form method="POST" action="/posts/{{$post->id}}/comment">
				@csrf
				<div class="modal-body form-group">
					<label for="comment">Comment:</label>
					<textarea name="comment" id="comment" type="text" class="form-control"></textarea>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary">Post Comment</button>
				</div>
				</form>
			</div>
		</div>
	</div>	

@endsection