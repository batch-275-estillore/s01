{{-- Activity for s02 --}}
@extends('layouts.app')

@section('content')
    <div class="text-center container">
        <img class="img-fluid" src="https://cdn.freebiesupply.com/logos/large/2x/laravel-1-logo-png-transparent.png">
        <h3 class="my-3">Featured Posts:</h3>
    </div>
    @if(count($posts)>0)
        @foreach($posts as $post)
            <div class="card text-center my-2">
                <div class="card-body">
                    <h4 class="card-title mb-3">
                        <a href="/posts/{{$post->id}}">
                            {{$post->title}}
                        </a>
                    </h4>
                    <h6 class="card-text mb-3">
                        Author: {{$post->user->name}}
                    </h6>
                </div>
            </div>
        @endforeach
    
    @else
        <div>
            <h2>There are no posts to show.</h2>
        </div>
    @endif
@endsection