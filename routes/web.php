<?php

use Illuminate\Support\Facades\Route;
//link the PostController file
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/posts/create', [PostController::class, 'create']);


Route::post('/posts', [PostController::class, 'store']);

//define a route that will return a view containing all the posts.
Route::get('/posts', [PostController::class, 'index']);

//Activity s02
// define a route that will return a view for the welcome page.
Route::get('/', [PostController::class, 'welcome']);

//define a route that will return a view containing only the authenticated user's posts
Route::get('/myPosts', [PostController::class, 'myPost']);

//define a route wherein a view showing a specific post with matching URL parameter ID ({}) will be returned to the user
Route::get('/posts/{id}', [PostController::class, 'show']);

//Activity s03
Route::get('posts/{id}/edit', [PostController::class, 'edit']);

Route::put('posts/{id}', [PostController::class, 'update']);


//Activity s04
Route::delete('/posts/{id}', [PostController::class, 'archive']);

//define a route that will call the like action
Route::put('/posts/{id}/like', [PostController::class, 'like']);

Route::post('/posts/{id}/comment', [PostController::class, 'comment']);
